import os
from flask import (
    Flask,
    g,
    session,
    redirect,
    request,
    url_for,
    jsonify,
    render_template,
)
from requests_oauthlib import OAuth2Session
import requests
import json
import sqlite3
import datetime
import humanize
from flask_babel import Babel, gettext, ngettext, _

from app import app

with open("./data/config.json") as json_data_file:
    cfg = json.load(json_data_file)

LANGUAGES = {
    "en": "English",
    "cs_CZ": "Czech",
    "nl_NL": "Dutch",
    "ru_RU": "Russian",
    "fr_FR": "French",
    "de_DE": "German",
    "az_AZ": "Azerbaijani",
    "hu_HU": "Hungarian",
    "pl_PL": "Polish",
    "es_ES": "Spanish",
    "ar_SA": "Arabic",
    "tr_TR": "Turkish",
}


@app.context_processor
def inject_languages():
    return dict(lang=LANGUAGES)


TEMPLATES_AUTO_RELOAD = cfg["TEMPLATES_AUTO_RELOAD"]
OAUTH2_CLIENT_ID = cfg["OAUTH2_CLIENT_ID"]
OAUTH2_CLIENT_SECRET = cfg["OAUTH2_CLIENT_SECRET"]
OAUTH2_REDIRECT_URI = cfg["OAUTH2_REDIRECT_URI"]
API_BASE_URL = "https://discordapp.com/api"
AUTHORIZATION_BASE_URL = API_BASE_URL + "/oauth2/authorize"
TOKEN_URL = API_BASE_URL + "/oauth2/token"
WEBHOOK_CHANNEL = cfg["WEBHOOK_CHANNEL"]
WEBHOOK_TOKEN = cfg["WEBHOOK_TOKEN"]
WEBHOOK_URL = API_BASE_URL + "/webhooks/" + WEBHOOK_CHANNEL + "/" + WEBHOOK_TOKEN
DASHBOARD_KEY = cfg["DASHBOARD_KEY"]
WARGAMING_TOKEN = cfg["WargamingToken"]

# app = Flask(__name__)
app.debug = cfg["FLASK_DEBUG"]
app.config["SECRET_KEY"] = cfg["SECRET_KEY"]

babel = Babel(app)

if "http://" in OAUTH2_REDIRECT_URI:
    os.environ["OAUTHLIB_INSECURE_TRANSPORT"] = cfg["OAUTHLIB_INSECURE_TRANSPORT"]


@babel.localeselector
def get_locale():
    # return request.accept_languages.best_match(['de', 'fr', 'en'])
    # return request.accept_languages.best_match(LANGUAGES.keys())
    # return "cs_CZ"
    try:
        language = session["language"]
    except KeyError:
        language = None
    if language is not None:
        if language in LANGUAGES:
            return language
    return request.accept_languages.best_match(LANGUAGES.keys())


@app.route("/language/<language>")
def set_language(language=None):
    if language in LANGUAGES:
        session["language"] = language
    return redirect(url_for("index"))


@babel.timezoneselector
def get_timezone():
    user = getattr(g, "user", None)
    if user is not None:
        return user.timezone


def token_updater(token):
    session["oauth2_token"] = token


def make_session(token=None, state=None, scope=None):
    return OAuth2Session(
        client_id=OAUTH2_CLIENT_ID,
        token=token,
        state=state,
        scope=scope,
        redirect_uri=OAUTH2_REDIRECT_URI,
        auto_refresh_kwargs={
            "client_id": OAUTH2_CLIENT_ID,
            "client_secret": OAUTH2_CLIENT_SECRET,
        },
        auto_refresh_url=TOKEN_URL,
        token_updater=token_updater,
    )


@app.route("/")
def index():
    # discord = make_session(token=session.get('oauth2_token'))

    if not session.get("username", False):
        return render_template("index_guest.html")

    user_id = session.get("user_id")
    with sqlite3.connect("./data/database.db") as con:
        cur = con.cursor()
        # con.row_factory = sqlite3.Row
        cur.execute("select * from data where id=?", (user_id,))
        row = cur.fetchone()
    if row:
        # return render_template('index.html',id=json.loads(row[0]),user=json.loads(row[1]),guilds=json.loads(row[2]),blitzquiz=json.loads(row[4]))
        storeddata = {}
        date = json.loads(row[2]).get("date", None)
        data = json.loads(row[2]).get("data", "")
        storeddata["date"] = date
        storeddata["data"] = data
        if date:
            datetime_object = datetime.datetime.strptime(date, "%Y-%m-%d %H:%M:%S.%f")
            storeddata["date"] = humanize.naturaltime(datetime_object)

        return render_template(
            "index.html",
            userid=json.loads(row[0]),
            playerconfig=json.loads(row[1]),
            storeddata=storeddata,
            pageconfig=json.loads(row[3]),
        )
    else:
        return redirect(url_for("me"))


@app.route("/widget/<id>")
def widget(id):
    if not id:
        return render_template("widget_not.html")
    with sqlite3.connect("./data/database.db") as con:
        print(id)
        cur = con.cursor()
        # con.row_factory = sqlite3.Row
        cur.execute("select * from data where id=?", (id,))
        row = cur.fetchone()
    if row:
        return render_template(
            "widget.html",
            userid=json.loads(row[0]),
            playerconfig=json.loads(row[1]),
            storeddata=json.loads(row[2]),
            pageconfig=json.loads(row[3]),
            token=WARGAMING_TOKEN,
        )
    else:
        return render_template("widget_not.html")


@app.route("/logout")
def logout():
    # discord = make_session(token=session.get('oauth2_token'))
    if session.get("oauth2_state", None):
        session.pop("oauth2_state")
    if session.get("oauth2_token", None):
        session.pop("oauth2_token")
    if session.get("username", None):
        session.pop("username")
    if session.get("user_id", None):
        user_id = session.pop("user_id")
    return redirect(url_for("index"))


@app.route("/login")
def login():
    scope = request.args.get("scope", "identify")
    discord = make_session(scope=scope.split(" "))
    authorization_url, state = discord.authorization_url(AUTHORIZATION_BASE_URL)
    session["oauth2_state"] = state
    return redirect(authorization_url)


@app.route("/callback")
def callback():
    if request.values.get("error"):
        return redirect(url_for("index"))
        # return request.values['error']
    discord = make_session(state=session.get("oauth2_state"))
    try:
        token = discord.fetch_token(
            TOKEN_URL,
            client_secret=OAUTH2_CLIENT_SECRET,
            authorization_response=request.url,
        )
        session["oauth2_token"] = token
        return redirect(url_for("me"))
    except:
        return redirect(url_for("index"))


@app.route("/receiver", methods=["POST"])
def receiver():
    data = request.get_json()
    print("received", data)
    if data:
        data_key = data.get("dashboard_key", False)
        if data_key == DASHBOARD_KEY:
            with sqlite3.connect("./data/database.db") as con:
                cur = con.cursor()
                cur.execute(
                    "INSERT or REPLACE INTO data (id,playerconfigs,guilds,guilds_invite,blitzquiz) VALUES (?,?,?,?,?)",
                    (
                        data["user_id"],
                        json.dumps(data["playerconfigs"]),
                        json.dumps(data["guilds"]),
                        json.dumps(data["guilds_invite"]),
                        json.dumps(data["blitzquiz"]),
                    ),
                )
                con.commit()

    return "OK"


@app.route("/me")
def me():
    discord = make_session(token=session.get("oauth2_token"))
    user = discord.get(API_BASE_URL + "/users/@me").json()
    if user.get("username", False):
        session["username"] = user.get("username")
        session["user_id"] = user.get("id")
    else:
        return redirect(url_for("login"))

    if session.get("user_id", None):
        user_id = session.get("user_id")
        if user_id:
            with sqlite3.connect("./data/database.db") as con:
                cur = con.cursor()
                cur.execute("select * from data where id = (?)", (user_id,))
                row = cur.fetchone()
                if not row:
                    print("now insert fresh data")
                    cur.execute(
                        "INSERT INTO data (id,playerconfig,storeddata,pageconfig) VALUES (?,?,?,?)",
                        (
                            user_id,
                            '{"player":"","region":""}',
                            "{}",
                            '{"battles":"Battles:","damage":"Damage:","winrate":"Winrate:", "style":".main {\\nbackground-color:green;\\n}\\n.streamtitle {\\n    background-color: green;\\n}\\n.battles {\\n    background-color: green;\\n}\\n.battles-text {\\n    background-color: green;\\n}\\n.damage-text {\\n    background-color: green;\\n}\\n.damage {\\n    background-color: green;\\n}\\n.winrate-text {\\n    background-color: green;\\n}\\n.winrate{\\n    background-color: green;\\n}\\n* {\\n    font-family: arial;\\n    font-size: 120%;\\n  }"}',
                        ),
                    )
                con.commit()

    return redirect(url_for("user"))


@app.route("/meold")
def meold():
    discord = make_session(token=session.get("oauth2_token"))
    user = discord.get(API_BASE_URL + "/users/@me").json()
    guilds = discord.get(API_BASE_URL + "/users/@me/guilds").json()
    # print(guilds)
    # connections = discord.get(API_BASE_URL + '/users/@me/connections').json()
    # return jsonify(user=user, guilds=guilds, connections=connections)
    if user.get("username", False):
        session["username"] = user.get("username")
        session["user_id"] = user.get("id")
    else:
        return redirect(url_for("login"))

    if session.get("user_id", None):
        user_id = session.get("user_id")
        if user_id:
            with sqlite3.connect("./data/database.db") as con:
                cur = con.cursor()
                cur.execute("update data set deleted = 1 where id = (?)", (user_id,))
                con.commit()

    guilds_send = [
        {"id": x["id"], "name": x["name"]}
        for x in guilds
        if x["owner"] or x["permissions"] >> 3 & 1
    ]
    session["guilds"] = guilds_send
    content = {
        "command": "get-data",
        "username": session["username"],
        "dashboard_key": DASHBOARD_KEY,
        "user_id": session["user_id"],
        "guilds": guilds_send,
    }

    payload = {"username": "Dashboard", "content": json.dumps(content)}
    requests.post(WEBHOOK_URL, data=payload)

    return redirect(url_for("edit"))


def reset_storage(user_id):
    print("reset data")
    with sqlite3.connect("./data/database.db") as con:
        cur = con.cursor()
        # con.row_factory = sqlite3.Row
        cur.execute("select * from data where id=?", (user_id,))
        row = cur.fetchone()
        if row:
            print("DATA", user_id, row[1])
            playerconfig = json.loads(row[1])
            player_id = playerconfig.get("id", None)
            region = playerconfig.get("region", None)

            if not (player_id and region):
                return redirect(url_for("me"))
            url = "https://api.wotblitz.{region}/wotb/account/info/?application_id={token}&account_id={player_id}&extra=statistics.rating".format(
                region=region.replace("na", "com"),
                token=WARGAMING_TOKEN,
                player_id=player_id,
            )
            r = requests.get(url)
            if r.status_code != 200:
                return redirect(url_for("index"))
            wg_data = r.json()
            print(wg_data)
            try:
                # wg_all=wg_data.get("data",None).get(str(player_id),None).get("statistics",None).get("all",None)
                wg_all = (
                    wg_data.get("data", None)
                    .get(str(player_id), None)
                    .get("statistics", None)
                )
            except:
                print("no data on wg", wg_data)
                wg_all = None

            print("all", wg_all)
            if not wg_all:
                cur.execute(
                    "update data set storeddata=? where id = ?",
                    (
                        json.dumps(
                            {"date": datetime.datetime.now(), "data": {}}, default=str
                        ),
                        user_id,
                    ),
                )
                return redirect(url_for("index"))
            print("update data")
            cur.execute(
                "update data set storeddata=? where id = ?",
                (
                    json.dumps(
                        {"date": datetime.datetime.now(), "data": wg_all}, default=str
                    ),
                    user_id,
                ),
            )
            con.commit()
            return redirect(url_for("index"))
        else:
            return redirect(url_for("me"))


@app.route("/reset", methods=["POST"])
def reset():
    if request.method == "POST":
        if not session.get("username", False):
            return redirect(url_for("login"))

        user_id = session.get("user_id")

        data = request.get_json(force=True)
        if data:
            if data["command"] == "reset":
                reset_storage(user_id)
        return "NOT OK"


@app.route("/data", methods=["POST"])
def data():
    if request.method == "POST":
        reset_data = False
        if not session.get("username", False):
            return redirect(url_for("login"))

        user_id = session.get("user_id")

        data = request.get_json(force=True)
        print("mam:", user_id, data, data["playerconfig"])
        with sqlite3.connect("./data/database.db") as con:
            cur = con.cursor()

            cur.execute("select * from data where id=?", (user_id,))
            row = cur.fetchone()
            if row:
                playerconfig_old = json.loads(row[1])
                playerconfig_new = data["playerconfig"]
                print(playerconfig_old)
                print(playerconfig_new)

                if (
                    playerconfig_old.get("region", None)
                    != playerconfig_new.get("region", None)
                ) or (
                    playerconfig_old.get("id", None) != playerconfig_new.get("id", None)
                ):
                    print("bude reset")
                    reset_data = True

        if data:
            with sqlite3.connect("./data/database.db") as con:
                cur = con.cursor()
                # con.row_factory = sqlite3.Row
                cur.execute(
                    "update data set playerconfig=?,pageconfig=? where id = ?",
                    (
                        json.dumps(data["playerconfig"]),
                        json.dumps(data["pageconfig"]),
                        user_id,
                    ),
                )
                con.commit()
                row = cur.fetchone()
        if reset_data:
            reset_storage(user_id)

        return "NOT OK"


@app.route("/user")
def user():
    if not session.get("username", False):
        return redirect(url_for("login"))

    user_id = session.get("user_id")
    with sqlite3.connect("./data/database.db") as con:
        cur = con.cursor()
        # con.row_factory = sqlite3.Row
        cur.execute("select * from data where id=?", (user_id,))
        row = cur.fetchone()
    if row:
        return render_template(
            "user.html",
            userid=json.loads(row[0]),
            playerconfig=json.loads(row[1]),
            pageconfig=json.loads(row[3]),
        )
    else:
        return redirect(url_for("me"))


if __name__ == "__main__":
    app.run()
